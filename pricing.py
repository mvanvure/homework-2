# -*- coding: utf-8 -*-
"""
Created on Tue Feb 11 11:52:11 2014

@author: dgevans
"""
import numpy as np

def riskFreePrice(Pi,lamb,gamma,beta):
    '''
    Computes the price of a risk free bond
    
    Inputs
    -------------
    * Pi - Transition matrix (SxS array)
    * lamb - growth rate of consumption (S array)
    * gamma - coefficient of risk aversion (float)
    * beta - discount factor (float)
    
    Returns
    ---------
    * pf - Price of the risk free bond for each state of the world (length S array)
    * Rf - Return on the risk free bond for each state of the world
    '''
    pf = beta*Pi.dot(lamb**(-gamma))
    Rf = 1/pf
    return pf, Rf
    
def stockPrice(Pi,lamb,gamma,beta):
    '''
    Computes the price to dividend ratio nu, and return on stock
    
    Inputs
    --------------
    * Pi - Transition matrix (SxS array)
    * lamb - growth rate of consumption (S array)
    * gamma - coefficient of risk aversion (float)
    * beta - discount factor (float)
    
    Returns
    ----------
    * nu - Price to dividend ratio for each state of the world
    * Rs - Return on the stock from state s to sprime
    
    S = len(lamb)
    A = np.eye(S) - (beta*(lamb**(1-gamma))*Pi)
    A = np.linalg.inv(A)
    B = beta*Pi.dot(lamb**(-gamma))
    nu = A.dot(B)
    Rs = np.ones([S,S])
    for i in range(S):
        for j in range(S):
            Rs[i,j] = ((nu[j]+1)/nu[i])*(lamb[j])
    return nu,Rs
    '''
    S = len(lamb)
    A = np.eye(S) - (beta*(lamb**(1-gamma))*Pi)
    A = np.linalg.inv(A)
    B = (beta*Pi*(lamb**(1-gamma))).dot(np.ones(S))
    nu = A.dot(B)
    Rs = np.ones([S,S])
    for i in range(S):
        for j in range(S):
            Rs[i,j] = ((nu[j]+1)/nu[i])*(lamb[j])
    return nu,Rs
    
def consolPrice(Pi,lamb,gamma,beta,zeta):
    '''
    Computes price of a consol bond with coupon zeta
    
    Inputs
    --------
    * Pi - Transition matrix (SxS array)
    * lamb - growth rate of consumption (S array)
    * gamma - coefficient of risk aversion (float)
    * beta - discount factor (float)
    * zeta - coupon (float)
    
    Returns
    ---------
    * pc - price of the consol in each state (length S array)
    * Rc - Return on the consol in each state (length S array)
    '''
    S=len(lamb)
    d = np.ones(S)*zeta
    A= np.eye(S)-(beta*Pi*(lamb**(-gamma)))
    A = np.linalg.inv(A)
    B = (beta*Pi*(lamb**(-gamma))).dot(d)
    pc = A.dot(B)
    Rc = np.ones([S,S])
    for i in range(S):
        for j in range(S):
            Rc[i,j] = (pc[j]+d[j])/pc[i]
    return pc,Rc
    
def callOption(Pi,lamb,gamma,beta,zeta,pStrike,T, epsilon = 1e-8):
    '''
    Computes price of a call option on a consol bond with payoff zeta
    
    Inputs
    --------
    * Pi - Transition matrix (SxS array)
    * lamb - growth rate of consumption (S array)
    * gamma - coefficient of risk aversion (float)
    * zeta - coupon (float)
    * pStrike - strike price (float)
    * T - length of option (can be inf)
    * epsilon - tolerance for infinite horizon problem (float) (bonus question)
    
    Returns
    --------
    * w - price of option in each state of the world
    '''
    S = len(lamb)
    w = np.zeros([T+1,S])
    pc,Rc = consolPrice(Pi,lamb,gamma,beta,zeta)
    for i  in range(1,T+1):
        temp=(beta*(lamb**(-gamma))*Pi).dot(w[i-1,:])
        for j in range(S):
            w[i,j]=max(temp[j],pc[j]-pStrike)
    return w[T,:]
    
    
def estimateEquityPremium(Pi_data,lambda_data,gamma,beta,sHist):
    '''
    Estimates the equity premium
    
    Inputs:
    --------
    * Pi_data : estimated transition matrix
    * lambda_data: estimated consumption growth
    * gamma : degree of risk aversion
    * beta : discount rate 
    * sHist : sample path of states
    
    Returns:
    -----------
    * EP : estimate of equity premium
    '''
    S = len(sHist)
    
    RiskFreeReturn = np.zeros(S-1) #Generate Risk Free Returns
    for t in range(S-1):
        pc, Rc = riskFreePrice(Pi_data,lambda_data,gamma,beta)
        RiskFreeReturn[t] = Rc[sHist[t]]
        
    RiskyReturn = np.zeros(S-1) #Generate Risky Returns
    for t in range(S-1):
       nu, Rs =  stockPrice(Pi_data,lambda_data,gamma,beta)
       #RiskyReturn[t] = Rs[sHist[t]][sHist[t+1]]#First defition of avg return
       mean = 0
       for j in range(len(Rs[sHist[t]])):
           mean += .1*(Rs[sHist[t]][j])
       RiskyReturn[t] = mean
       
    MeanRiskFree = 0#compute mean
    for i in range(S-1):
        MeanRiskFree += RiskFreeReturn[i]
    MeanRiskFree = MeanRiskFree/(S-1)
    
    MeanRisky = 0 #compute mean
    for i in range(S-1):
        MeanRisky += RiskyReturn[i]
    MeanRisky = MeanRisky/(S-1)
    
    return ((MeanRisky) - (MeanRiskFree)) #compute the annualized difference and return
    
    
