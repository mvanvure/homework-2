# -*- coding: utf-8 -*-
"""
Created on Tue Feb 11 12:17:17 2014

@author: dgevans
"""
import numpy as np
import pricing as AP
import pandas as pd
import pandas.io.data as web
import datetime
import matplotlib.pyplot as plt


##################
# Question 1     #
##################

#part a.

S = 5
Pi = 0.0125*np.ones((5,5))
Pi += np.diag(0.95-0.0125*np.ones(5))
#construct lambda
lamb = np.array([1.025,1.015,1.,0.985,0.975])
#gamma
gamma = 2.
#beta
beta = 0.96

pf,Rf = AP.riskFreePrice(Pi,lamb,gamma,beta)
print ("Risk Free Bond Price and return: ")
print (pf)
print (Rf)



#part b

nu,R = AP.stockPrice(Pi,lamb,gamma,beta)
print ("Stock price and return: ")
print (nu)
print (R)



#part c

zeta = 1.
pc,Rc = AP.consolPrice(Pi,lamb,gamma,beta,zeta)
print ("Consol Bond Price and Return: ")
print (pc)
print (Rc)



#part d

p_s = 30.
w = AP.callOption(Pi,lamb,gamma,beta,zeta,p_s,20)
print ('Price of Call Option: ')
print (w)



##################
# Question 2     #
##################

start = datetime.datetime(1955, 1, 1)
end = datetime.datetime(2013, 1, 1)
df = web.DataReader("PCECC96", "fred", start, end)#get data

#part a
df.columns = ['C']#rename to C

df['L1C']=df.C.shift(1)#generate growth rate
df['c_growth'] = (df['C'])/df['L1C']

df.drop(df.index[0],inplace=True)#drop first row missing values

df['s']=pd.qcut(df['c_growth'],10,labels = [0,1,2,3,4,5,6,7,8,9])#create s

#part b
lambda_data = np.zeros(10)
temp = df.groupby('s').mean()['c_growth']
for i in range(10):
    lambda_data[i] = temp[i]
    
Pi_data = np.ones([10,10])*.1

#part c

gamma_vec = np.linspace(0,20,50)
beta = .995
sHist = df['s']
G = len(gamma_vec)
EquityPrimum_vec = np.zeros(G)
for i in range(G):
    EquityPrimum_vec[i] = AP.estimateEquityPremium(Pi_data,1+(4*(lambda_data-1)), gamma_vec[i], beta, sHist)
print('EquityPrimum_vec')
print(EquityPrimum_vec)

plt.plot(gamma_vec,EquityPrimum_vec)